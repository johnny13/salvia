
     _______ _______ _____   ___ ___ _______ _______ 
    |     __|   _   |     |_|   |   |_     _|   _   |
    |__     |       |       |   |   |_|   |_|       |
    |_______|___|___|_______|\_____/|_______|___|___|
    -----------------------------------[ 0.0.1 ]-----
    
# The Diviner's Sage
Salvia is a custom Wordpress codebase. 

Some of its most noteable features include:
+ a full fledged develop environment
+ task runner
+ asset builder
+ speed optimizations
+ lightly modified version of the Sage 9 theme.
+ staging and production deployment scripts


## Developer Friendly
There is a prebuilt **Vagrant** box setup that comes packaged with Saliva.

It allows you to quickly spin up **MULTIPLE** Wordpress sites on a single VM.

Quickly switch between PHP versions and much more, allowing you to not only dev, but also future proof certain features. 

